# How to build and deploy production 

* Once you have passed the test on staging version, you can build production version by
```
make build-production
```
* Copy the `dist` to another location where you can import as another project in mini program IDE.
* Upload the package through IDE.
* Scan the code to use.
