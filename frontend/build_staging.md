# How to build and deploy staging 


### Test staging version
After you develop with local `est-backend`, and make sure the changes on `est-frontend` is valid. Connect with staging `est-backend` by
```
make start-staging
```
Test on IDE simulator.

### Build staging version
```
make build-staging
```
Once the dist is built, you can upload the package as a trial version, and scan the code to test the staging version.

