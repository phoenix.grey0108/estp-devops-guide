# Create development environment

### For debian  (NOT recommend)
If you OS is `ubuntu` or `debian`, please execute
```
make prepare-for-debian
```

### Install 
```
make install-dependecy
```

### Start dev environment
* Install wechat mini program IDE, importing project from `dist`
* Start sync service
```
make start-dev
```
Which will connect to the local `est-backend`, make sure you have one started. You can modify the `BK_SERVER` in `src/config/config.development.js`.
