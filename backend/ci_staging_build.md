# How to build with CI

### Utilities
* We use gitlab ci to build docker image called `ntsp.joybin.cn:5000/est-backend`, which is stored in private docker registry in `aliyun ECS`.
* The gitlab-runner is installed and started in `aliyun ECS`.
* Just commit the code on branch `devops` and push to gitlab, the CI process will be on automatically.
* You can check the `CI/CD` on the backend project page.

### How to build
* Commit on branch `devops`. 
* Push to gitlab.
* Wait till the build end:
  * `ntsp.joybin.cn:5000/est-backend:staging` will be generated.
  * `deployment-staging.zip` will be generated.
  * `nginx-staging.zip` will be generated.
  * The docker deployment will be done automatically.
  * The nginx has been deployed, and will be skipped. If you want to make changes of nginx deployment, remove the dot before `deploy_nginx` in `.gitlab-ci.yml`. 

### Prepare redis
```
vim /etc/redis/redis.conf
```

* Change `requirepass` refer to `CACHE_PASSOWRD` in `staging.env`.
* Uncomment `bind 127.0.0.1`.

```
sudo systemctl restart redis-server
```

**Be Aware**:

* There are two redis server in this project.
* one is deployed as a docker container, port `6389`, which is for calculating report.
* The other is deployed bare metal with password, port `6379`, which is for caching result.

### How to test
* Visit `ntsp.joybin.cn:4000/nest/apidocs` in browser.
* Use `Postman` to test the API with `ESTP-DEV` environment.
* If the nginx is deployed, the `ESTP-DEV` should point to port `8080`, otherwise you need to change into `4000`.
