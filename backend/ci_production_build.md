# How to build with CI

### Utilities
* We use gitlab ci to build docker image called `ntsp.joybin.cn:5000/est-backend`, which is stored in private docker registry in `aliyun ECS`.
* The gitlab-runner is installed and started in `aliyun ECS`.
* Just commit and tag the code and push to gitlab, the CI process will be on automatically.
* You can check the `CI/CD` on the backend project page.

### How to build
* Tag the commit with `semver`, e.g, `0.0.2`.
* Push to gitlab
* Wait till the build end:
  * `ntsp.joybin.cn:5000/est-backend:0.0.2` will be generated.
  * `deployment-0.0.2.zip` will be generated.
  * `nginx-0.0.2.zip` will be generated.
  * The docker deployment will be done automatically.
  * The nginx has been deployed, and will be skipped. If you want to make changes of nginx deployment, remove the dot before `deploy_nginx` in `.gitlab-ci.yml`. 

### Prepare redis
```
vim /etc/redis/redis.conf
```

* Change `requirepass` refer to `CACHE_PASSOWRD` in `production.env`.
* Uncomment `bind 127.0.0.1`.

```
sudo systemctl restart redis-server
```

**Be Aware**:

* There are two redis server in this project.
* one is deployed as a docker container on swarm manager, port `6389`, which is for calculating report.
* The other is deployed bare metal on one of swarm workers, with password, port `6379`, which is for caching result.


### How to test
* Use `Postman` to test the API with `ESTP-PROD` environment.
* If the nginx is deployed, the `ESTP-PROD` should point to port `8080`.
* The `4000` is not exposed outside.
