# Create development environment

### Clone the project
```
git clone https://gitlab.com/phoenix.grey0108/est-backend.git
cd est-backend
```

### For debian
If you OS is `ubuntu` or `debian`, please execute
```
make prepare-for-debian
```

### Install pyenv
```
make install-pyenv
```

### Create python env
```
make create-devenv
```

### Config redis
```
sudo vim /etc/redis/redis.conf
```

* Change `requirepass` refer to `CACHE_PASSOWRD` in `development.env`.
* Uncomment `bind 127.0.0.1`.

```
sudo systemctl restart redis-server
```

### Deploy nginx
```
make deploy-dev-nginx
```

### Start backend service
```
make start FLASK_ENV=development
```
The configuration of development is in the file called `development.env`.

### Test API
* You can visit `127.0.0.1:4000/nest/apidocs` in browser.
* You can use `Postman` to test the API, and remember to choose the `ESTP-LOCAL`. 
* If the nginx is deployed, the `ESTP-LOCAL` should point to port `8080`, otherwise you need to change into `4000`.
