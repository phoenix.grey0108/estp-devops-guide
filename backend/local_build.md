# How to build development version locally

### Build the docker image locally
```
make build-image FLASK_ENV=development
```
The docker image called `ntsp.joybin.cn:5000/est-backend:dev` is created.

### Build the deployment scripts
```
make ci-create-deployment FLASK_ENV=development
```
* The `nginx-dev.zip` is generated for nginx deployment. It is optinal to use.
* The `deployment-dev.zip` is generated for docker deployment.

### Config redis
You should have configured redis refer to [Config redis](create_devenv.md) 

### Test the docker image
```
cd docker-deployment
make config
make deploy
```
* Make sure that you have installed `docker-compose` and done `docker swarm init`.
* Docker container is started.
* The http RESTful API will be able at `0.0.0.0:4000`.
* Visit `127.0.0.1:4000/nest/apidocs` in browser.
* Use `Postman` to test the API, and remember to choose the `ESTP-LOCAL`.
* If the nginx is deployed, the `ESTP-LOCAL` should point to port `8080`, otherwise `4000`.
