# Summary

* [Introduction](README.md)


### Devops for DB
* [Intro to db env](db/intro_devenv.md)
* [Prepare db](db/prepare_db.md)
* [Create db](db/create_db.md)
* [Import user data](db/import_user_data.md)
* [Import event data](db/import_event_data.md)

### Devops for PIB
* [Parse and import event data](pib/pib_event.md)
* [Parse and import user data](pib/pib_user.md)
* [Cleanse](pib/pib_cleanse.md)

### Devops for backend
* [Create dev env for backend](backend/create_devenv.md)
* [Build and deploy development version locally](backend/local_build.md)
* [Build and deploy staging vesion with CI](backend/ci_staging_build.md)
* [Build and deploy production vesion with CI](backend/ci_production_build.md)

### Devops for frontend
* [Create dev env for frontend](frontend/create_devenv.md)
* [Build and deploy staging version](frontend/build_staging.md)
* [Build and deploy production version](frontend/build_production.md)

