# Import event data from excel

### Prerequisites

#### Excel
* The excel files should use GB2312 as its encoding method. Incorrect encoding method will result in messsy code.

* The excel files should strictly follow the required format.

#### Windows
When running in windows, a .Net IDE (Visual Studio) is required. Community edition is acceptable.

After installing the IDE, please import the project under `est-pib-data/question-import/pib-net`. Then compile and run the project. A web interface should be automatically started. If not, please manually input `http://0.0.0.0:8090` in your browser.

#### Linux
When running in Linux, please first execute the build script.

```
est-pib-data/question-import/pib-net/pib-net/build-release.sh
```

And then excute the start script.

```
est-pib-data/question-import/pib-net/pib-net/start-server.sh
```

After the server is successfully started, please manually input `http://0.0.0.0:8090` or `http://[Your server ip]:8090` in your browser.


### Import event data to the database
On the web interface, click `choose files` to choose the excel files and click the button `Import` to import. 

**Please do not exit the page before it refreshes itself.**



