# Import user data from pib

### Download user data
The following command will download the user data from the remote source. The data includes student, teacher and organization data.

```
make download-data
```

### Import user data to the database
The following command will analyze the downloaded user data and import it into the database.

```
make analyse-data
```


