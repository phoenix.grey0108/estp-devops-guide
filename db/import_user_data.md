# Import user data 

* The user data are stored in tables `school`, `student`, `teacher`, `class` and `class_teacher_map`.
* The initial data in `class_teacher_map` is empty.

There are two approaches to import user data into mysql server.

### Import from PIB
You can use the project called `est-pib-data` to import user data into mysql, please refer to [Import user data from pib](../pib/pib_user.md).

### Import from dumped data
Firstly, make sure that you have already dumped the user data once you import them from pib api. Since in general, the data don't need to be modified.

```
make recover-g-user SERVER=xxxx
```

Choose the `SERVER` value according to the environment.

