# Create db environment

You need to log in to the machine where you are going to deploy db on to perform all the tasks.

### Log into the machine
```
ssh developer@[MACHINE]
```

### Install mysql in your machine


### Clone project
```
git clone https://gitlab.com/phoenix.grey0108/est-db.git
cd est-db
```

### Update root password
```
make update-root-password
```

### Create a new user
```
make create-db-user
```

