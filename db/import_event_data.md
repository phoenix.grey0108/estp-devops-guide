# Import event data

The event data are stored in tables `event`, `lc`, `readrepeat`. There are two approaches to import event data into mysql server.

### Import from excel
You can use the project called `est-pib-data` to import event data into mysql, please refer to [Import event data from excel](../pib/pib_event.md).

### Import from dumped data
Firstly, make sure that you have already dumped the event data once you import them from excel. Since in general, the data don't need to be modified unless something incorrect with the original data in excel.

```
make recover-g-event SERVER=xxxx
```

Choose the `SERVER` value according to the environment.

