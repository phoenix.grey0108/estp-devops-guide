# Introduction to DB environment

* You can deploy mysql in any machine. In this project, the `SERVER` represents the `ip` or `domain` of mysql server, which can be differenticated among environments.

* We use three envs `local`, `development` and `production`. The detail information is described in `README.md` of project `est-db`.
