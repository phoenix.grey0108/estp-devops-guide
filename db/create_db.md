# Create DB

You can perform all the tasks on any machine from which you can access to the mysql server.

### Create schema
```
make create-schema SERVER=xxxx SQLUSER=yyyyy
```
which creates schema of db `nest`. 

* Default value of `SERVER` is `127.0.0.1` and default value of `SQLUSER` is `nestpj`.
* You should change the `SERVER` according to your environment, `local`, `development`, `production`.
* For more information about environment, you can check the `README.md` of `est-db`.
 
### Show data
```
make show-db SERVER=xxxx SQLUSER=yyyyy
```
which shows data of db `nest`.
